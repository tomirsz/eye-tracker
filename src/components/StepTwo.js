import {Button, TextField} from "@material-ui/core";
import '../styles/StepTwo.css';
import SimpleDialog from "./SimpleDialog";
import NumberIndicator from "./NumberIndicator";
import {useState} from "react";

const StepTwo = () => {
    const [openIndicators, setOpenIndicators] = useState(false);

    return (
        <div className="main_container_proper2">
            <SimpleDialog
                setOpenIndicators={setOpenIndicators}
                slideNumber={2}
                openDialog={true}
                title="Przykład dobrego designu Z - pattern"
                content="W tym przykładzie wzrok odwiedzającego stronę w pierwszej kolejności natrafia na logo (1), następnie na panel logowania (2), by następnie trafić informacje o stronie (3), a na samym końcu na formularz rejestracji (4). Kliknij dalej, żeby zobaczyć wzorzec F."
            />
            <div className="logo2">{
                openIndicators &&
                <NumberIndicator number={1}/>
            }
                <h2>LOGO</h2>
            </div>
            <div className="navbar2">{
                openIndicators &&
                <NumberIndicator number={2}/>
            }
                <TextField label="email" variant="outlined">Email</TextField>
                <TextField label="password" variant="outlined">Password</TextField>
                <Button variant="contained" color="primary">Log In</Button>
            </div>

            <div className="content_image2">{
                openIndicators &&
                <NumberIndicator number={3}/>
            }
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <div className="content2">{
                openIndicators &&
                <NumberIndicator number={4}/>
            }
                <TextField label="First Name" variant="outlined">First Name</TextField>
                <TextField label="Surname" variant="outlined">Surname</TextField>
                <TextField label="email" variant="outlined">Email</TextField>
                <TextField label="password" variant="outlined">Password</TextField>
                <Button variant="contained" color="primary">Create an account</Button>
            </div>
        </div>
    )
}

export default StepTwo