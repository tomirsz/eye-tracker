import {Button, TextField} from "@material-ui/core";
import img1 from "../assets/LOGO1.jpeg";
import img2 from "../assets/LOGO2.jpeg";
import img3 from "../assets/LOGO3.jpeg";
import img4 from "../assets/LOGO4.jpeg";
import '../styles/StepFour.css';
import SimpleDialog from "./SimpleDialog";
import NumberIndicator from "./NumberIndicator";
import {useState} from "react";

const StepFour = () => {
    const [openIndicators, setOpenIndicators] = useState(false);

    return (
        <div className="main_container4">
            <SimpleDialog
                setOpenIndicators={setOpenIndicators}
                slideNumber={4}
                openDialog={true}
                title="Przykład dobrego designu Z - pattern"
                content="W tym przykładzie wzrok odwiedzającego stronę w pierwszej kolejności natrafia na panel logowania (1), następnie na opis strony (2), a na samym końcu na loga partnerów (3). Informacje są ułożone prawidłowo, od najwazniejszej na górze, do najmniej istotnej na dole strony."
            />
            <div className="navbar4">{
                openIndicators &&
                <NumberIndicator number={1}/>
            }
                <h2>LOGO</h2>
                <TextField label="email" variant="outlined">Email</TextField>
                <TextField label="password" variant="outlined">Password</TextField>
                <Button variant="contained" color="primary">Log In</Button>
            </div>
            <div className="content4">{
                openIndicators &&
                <NumberIndicator number={2}/>
            }
                <h1>Lorem ipsum dolor sit amet...</h1>
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula mauris consectetur, facilisis
                    neque hendrerit, porttitor nisl. </p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula mauris consectetur, facilisis
                    neque hendrerit, porttitor nisl. Sed et dignissim nunc. Donec justo nibh, vestibulum nec sem
                    vehicula,
                    viverra ullamcorper lorem. Vestibulum dignissim ipsum ut libero ultrices, eget pretium nisi aliquet.
                    Suspendisse vitae rhoncus elit. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
            </div>
            <div className="footer4">{
                openIndicators &&
                <NumberIndicator number={3}/>
            }
                <img src={img1} alt="img1"/>
                <img src={img2} alt="img2"/>
                <img src={img3} alt="img3"/>
                <img src={img4} alt="img4"/>
            </div>
        </div>
    )
}

export default StepFour