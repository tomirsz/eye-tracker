import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Draggable from 'react-draggable';
import {Button} from "@material-ui/core";
import '../styles/SimpleDialog.css';
import {Link} from "react-router-dom";
import {useState} from "react";
import Paper from '@material-ui/core/Paper';

const PaperComponent = (props) =>  {
    return (
        <Draggable handle="#draggable-dialog-title" cancel={'[class*="MuiDialogContent-root"]'}>
            <Paper {...props} />
        </Draggable>
    );
}

const SimpleDialog = (props) => {
    const {title, content, slideNumber, openDialog, setOpenIndicators} = props;
    const [open, setOpen] = useState(false);

    let previousButton = <Button component={Link} to={`/step${slideNumber - 1}`}>Wstecz</Button>;
    let nextButton = <Button component={Link} to={`/step${slideNumber + 1}`}>Dalej</Button>;
    let endButton = '';


    setTimeout(function () {
        if (openDialog) {
            setOpen(true);
            setOpenIndicators(true)
        }
    }, 10000);

    if (slideNumber <= 1) {
        previousButton = ''
    }

    if (slideNumber >= 4) {
        nextButton = ''
        endButton = <Button component={Link} to="/">Koniec</Button>;
    }

    return (
        <div>
            <Dialog aria-labelledby="simple-dialog-title"
                    open={open}
                    PaperComponent={PaperComponent}
                    BackdropProps={{ style: { backgroundColor: "transparent" } }}
            >
                <DialogTitle style={{ cursor: 'move' }} id="draggable-dialog-title">{title}</DialogTitle>
                <div className="content">
                    <p>{content}</p>
                </div>
                <div className="button_container">
                    {previousButton}
                    {nextButton}
                    {endButton}
                </div>
            </Dialog>
        </div>
    )
}

export default SimpleDialog