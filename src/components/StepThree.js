import {Button, TextField} from "@material-ui/core";
import img1 from '../assets/LOGO1.jpeg';
import img2 from '../assets/LOGO2.jpeg';
import img3 from '../assets/LOGO3.jpeg';
import img4 from '../assets/LOGO4.jpeg';
import '../styles/StepThree.css';
import SimpleDialog from "./SimpleDialog";
import NumberIndicator from "./NumberIndicator";
import {useState} from "react";

const StepThree = () => {
    const [openIndicators, setOpenIndicators] = useState(false);

    return (
        <div className="main_container_wrong3">
            <SimpleDialog
                setOpenIndicators={setOpenIndicators}
                slideNumber={3}
                openDialog={true}
                title="Przykład złego designu F - pattern"
                content="W tym przykładzie wzrok odwiedzającego stronę w pierwszej kolejności natrafia na opis strony (1), następnie na loga partnerów (2), a na samym końcu na panel logowania (3). Kliknij dalej, żeby zobaczyć prawidłowe rozmieszczenie elementów."
            />
            <div className="navbar3">{
                openIndicators &&
                <NumberIndicator number={3}/>
            }
                <h2>LOGO</h2>
                <TextField label="email" variant="outlined">Email</TextField>
                <TextField label="password" variant="outlined">Password</TextField>
                <Button variant="contained" color="primary">Log In</Button>
            </div>
            <div className="content3">{
                openIndicators &&
                <NumberIndicator number={1}/>
            }
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula mauris consectetur, facilisis
                    neque hendrerit, porttitor nisl. </p>
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                <h1>Lorem ipsum dolor sit amet...</h1>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vehicula mauris consectetur, facilisis
                    neque hendrerit, porttitor nisl. Sed et dignissim nunc. Donec justo nibh, vestibulum nec sem
                    vehicula,
                    viverra ullamcorper lorem. Vestibulum dignissim ipsum ut libero ultrices, eget pretium nisi aliquet.
                    Suspendisse vitae rhoncus elit. Interdum et malesuada fames ac ante ipsum primis in faucibus.</p>
            </div>
            <div className="footer3">{
                openIndicators &&
                <NumberIndicator number={2}/>
            }
                <img src={img1} alt="img1"/>
                <img src={img2} alt="img2"/>
                <img src={img3} alt="img3"/>
                <img src={img4} alt="img4"/>
            </div>
        </div>
    )
}

export default StepThree