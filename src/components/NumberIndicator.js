import '../styles/NumberIndicator.css';

const NumberIndicator = (props) => {
    const {number} = props;

    return (
        <div className="number_indicator">
            <h1>{number}</h1>
        </div>
    )
}
export default NumberIndicator