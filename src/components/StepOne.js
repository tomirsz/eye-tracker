import {Button, TextField} from "@material-ui/core";
import '../styles/StepOne.css';
import SimpleDialog from "./SimpleDialog";
import NumberIndicator from "./NumberIndicator";
import {useState} from "react";

const StepOne = () => {
    const [openIndicators, setOpenIndicators] = useState(false);

    return (
        <div className="main_container_wrong1">
            <SimpleDialog
                setOpenIndicators={setOpenIndicators}
                slideNumber={1}
                openDialog={true}
                title="Przykład złego designu Z - pattern"
                content="W tym przykładzie wzrok odwiedzającego stronę w pierwszej kolejności natrafia na formularz (1), następnie na opis strony (2), by następnie trafić na logo (3), a na samym końcu na panel logowania (4). Kliknij dalej, żeby zobaczyć prawidłowe rozmieszczenie elementów."
            />
            <div className="navbar1">
                <div className="logo1">{
                    openIndicators &&
                    <NumberIndicator number={3}/>
                }
                    <h2>LOGO</h2>
                </div>
                {
                    openIndicators &&
                    <NumberIndicator number={4}/>
                }
                <TextField label="email" variant="outlined">Email</TextField>
                <TextField label="password" variant="outlined">Password</TextField>
                <Button variant="contained" color="primary">Log In</Button>
            </div>

            <div className="content_image1"> {
                openIndicators &&
                <NumberIndicator number={2}/>
            }
                <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
            <div className="content1"> {
                openIndicators &&
                <NumberIndicator number={1}/>
            }
                <TextField label="First Name" variant="outlined">First Name</TextField>
                <TextField label="Surname" variant="outlined">Surname</TextField>
                <TextField label="email" variant="outlined">Email</TextField>
                <TextField label="password" variant="outlined">Password</TextField>
                <Button variant="contained" color="primary">Create an account</Button>
            </div>
        </div>
    )
}

export default StepOne