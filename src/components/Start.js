import Button from "@material-ui/core/Button"
import {Link} from "react-router-dom";
import '../styles/Start.css';


const Start = () => {
    return (
        <div className="start_container">
            <h1>Patterny F i Z</h1>
            <h2>Czyli wzorce konsumowania treści</h2>
            <p>Zapraszamy na krótką wirtualną podróż po optymalnym designie.</p>
            <p>Na każdym ekranie po upływie 10s pojawi się opis i będziesz mógł/mogła przejść dalej.</p>
            <p>Opis możesz przesuwać po ekranie, wystarczy, że klikniesz w tytuł i przesuniesz okienko.</p>
            <Button component={Link} to="/step1" variant="contained" color="primary">START</Button>
        </div>
    )
}

export default Start