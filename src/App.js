import './styles/App.css';
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import StepOne from "./components/StepOne";
import StepTwo from "./components/StepTwo";
import StepThree from "./components/StepThree";
import StepFour from "./components/StepFour";
import Start from "./components/Start";
import End from "./components/End";

function App() {
    return (
        <div className="app_container">
            <Router>
                <Switch>
                    <Route exact path="/">
                        <Start/>
                    </Route>
                </Switch>
                <Switch>
                    <Route exact path="/step1">
                        <StepOne/>
                    </Route>
                </Switch>
                <Switch>
                    <Route exact path="/step2">
                        <StepTwo/>
                    </Route>
                </Switch>
                <Switch>
                    <Route exact path="/step3">
                        <StepThree/>
                    </Route>
                </Switch>
                <Switch>
                    <Route exact path="/step4">
                        <StepFour/>
                    </Route>
                </Switch>
                <Switch>
                    <Route exact path="/end">
                        <End/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
